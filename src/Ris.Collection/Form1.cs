﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ris.Collection
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        #region 爬虫
        //要爬取的网址
        public string requestUrl = "http://channel.chinanews.com/cns/cl/cj-hgds.shtml" + "?pager=";
        public int pager = 0;
        #region 爬虫
        /// <summary>
        /// 加载URL
        /// </summary>
        /// <returns></returns>
        public string LoadUrl(string url, Encoding encoding)
        {
            //既然说到要使用 WebRequest 类，那就来一个
            WebRequest webRequest = WebRequest.Create(url);
            //ok啦，这就模仿我们向浏览器输入了一个网页，可是我们什么都看不见啊，那是因为我们没有得到响应，那就再用WebResponse类获得响应
            WebResponse webResponse = (WebResponse)webRequest.GetResponse();
            //到现在你还是什么都看不到，响应是有了，但是没有显示出来还是什么都没有的，我们定义一个流来保存获得的内容
            Stream responseStream = webResponse.GetResponseStream();
            StreamReader streamReader = new StreamReader(responseStream, encoding);
            #region 解惑
            //问题：等等，我们为什么要用流来保存内容，直接显示在textBox或者其他地方不可以吗？ 
            //回答：因为 response. 中能获得内容的只有 GetResponseStream()
            //追问：我看了一下，还有一个 toString() 啊
            //回答：Just trying。你会发现只是把 response 的数据类型显示了出来，这里可不是简单的类型转换

            //说个小故事，其实一开始我也是不知道为什么用流的，那就try咯， response.打完再看看有什么符合要求的，见建议1
            #endregion
            //有了“流”，还需要“读流”
            #region 解惑
            //问：StreamReader 里面的参数怎么写
            //答：里面的参数可以有很多，貌似最多9个（+9重载），这里的 dataStream 是流，第二个Encoding.Default是流的编码模式，在这里用的是默认模式
            //拓展：在使用流来保存和读取文本文件（.txt）时，貌似用的是Encoding.GetEncoding("GB2312")，才不会出现乱码
            #endregion
            //这个时候你就可以把reader好好折磨一番了
            string html = streamReader.ReadToEnd();

            return html;
        }

        public HtmlAgilityPack.HtmlDocument CreateHtmlDocument(string html)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);//博客主页URL

            return doc;
        }
        public bool JieXiHtml(string html)
        {
            //using (SDBContext dbContext = new SDBContext())
            //{
            //    var doc = this.CreateHtmlDocument(html);
            //    //下面的意思是：通过属性class的值，来定位节点信息
            //    HtmlNodeCollection htmlNodes = doc.DocumentNode.SelectNodes("//div[@class='con2']/table[@class='12v']");

            //    if (htmlNodes.Count == 0)
            //        return false;//不再进行抓取操作
            //    var htmlNodeList = htmlNodes.ToList();

            //    for (var i = 0; i < htmlNodeList.Count; i++)
            //    {
            //        var htmlNode = htmlNodeList[i];
            //        //在取页面table中的内容时，用到了循环，但是发现两次取出的内容赋值时，值相同，存在缓存问题。在网上查找后，需要做一下处理。
            //        //用temp而不是htmlNode再进行相关的赋值即可
            //        var temp = HtmlNode.CreateNode(htmlNode.OuterHtml);
            //        var news = new News();
            //        var titleHtmlNode = temp.SelectSingleNode("//tr[1]/td[1]/a");
            //        news.Title = titleHtmlNode == null ? "" : titleHtmlNode.InnerText;
            //        var dateHtmlNode = temp.SelectSingleNode("//tr[1]/td[2]");
            //        news.CreateTime = dateHtmlNode == null ? DateTime.Parse("1970-01-01 00:00:00") : DateTime.Parse(dateHtmlNode.InnerText);
            //        var descHtmlNode = temp.SelectSingleNode("//tr[2]/td[1]/font");
            //        news.Description = descHtmlNode == null ? "" : descHtmlNode.InnerText;
            //        var linkHtmlNode = temp.SelectSingleNode("//tr[3]/td[1]");
            //        news.Link = linkHtmlNode == null ? "" : linkHtmlNode.InnerText;

            //        if (!string.IsNullOrEmpty(news.Link) && news.Link.Contains("http://"))
            //        {
            //            var contentHtml = this.LoadUrl(news.Link, Encoding.Default);
            //            var contentDoc = this.CreateHtmlDocument(contentHtml);
            //            HtmlNode sourceHtmlNode = contentDoc.DocumentNode.SelectSingleNode("//div[@id='cont_1_1_2']/div[@class='left-time']/div[@class='left-t']");
            //            news.Source = sourceHtmlNode == null ? "" : sourceHtmlNode.InnerText;
            //            HtmlNode contentHtmlNode = contentDoc.DocumentNode.SelectSingleNode("//div[@id='cont_1_1_2']/div[@class='left_zw']");
            //            news.Content = contentHtmlNode == null ? "" : contentHtmlNode.InnerHtml;
            //            HtmlNode authorHtmlNode = contentDoc.DocumentNode.SelectSingleNode("//div[@id='cont_1_1_2']/div[@class='left_name']/div[@class='left_name']");
            //            news.Author = authorHtmlNode == null ? "" : authorHtmlNode.InnerText;
            //        }

            //        dbContext.News.Add(news);
            //    }
            //    try
            //    {
            //        dbContext.SaveChanges();
            //    }
            //    catch (Exception ex)
            //    {

            //    }

            //    this.pager += 1;

            //    return true;//继续爬取
            //}

            return false;
        }
        #endregion

        private void startBtn_Click(object sender, EventArgs e)
        {
            var isTrue = true;
            while (isTrue)
            {
                isTrue = this.Start(requestUrl + this.pager);
            }
        }

        public bool Start(string url)
        {
            var html = this.LoadUrl(url, Encoding.UTF8);
            return this.JieXiHtml(html);
        }
        #endregion
    }
}