﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ris.Library
{
    /// <summary>
    /// 公司
    /// </summary>
    public class Company : SuperNameClass
    {
        /// <summary>
        /// 企业性质
        /// </summary>
        public string Nature { get; set; }
        /// <summary>
        /// 营业类型
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 人数
        /// </summary>
        public string NumberOfPeople { get; set; }
        /// <summary>
        /// 公司地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 企业网址
        /// </summary>
        public string Website { get; set; }
        /// <summary>
        /// 公司概况
        /// </summary>
        public string Survey { get; set; }
    }
}
