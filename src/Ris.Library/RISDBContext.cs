﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ris.Library
{
    /// <summary>
    /// EF上下文
    /// </summary>
    public class RISDBContext : DbContext
    {
        /// <summary>
        /// 职位信息
        /// </summary>
        public DbSet<Job> Jobs { get; set; }
        /// <summary>
        /// 职位福利
        /// </summary>
        public DbSet<JobWelfare> JobWelfares { get; set; }
        /// <summary>
        /// 职位信息所提供的职位福利（职位信息和职位福利的中间表）
        /// </summary>
        public DbSet<Job_JobWelfare> Job_JobWelfares { get; set; }
        /// <summary>
        /// 公司
        /// </summary>
        public DbSet<Company> Companys { get; set; }
    }
}
