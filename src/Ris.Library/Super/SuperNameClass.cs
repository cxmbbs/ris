﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ris.Library
{
    /// <summary>
    /// 带有Name字段的超类
    /// </summary>
  public  class SuperNameClass : SuperAllowClass
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }
}
