﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ris.Library
{
    /// <summary>
    /// 带有修改时间的超类
    /// </summary>
    public class SuperAllowClass : SuperClass
    {
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }
    }
}
