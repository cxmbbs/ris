﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ris.Library
{
    /// <summary>
    /// 职位
    /// </summary>
    public class Job : SuperAllowClass
    {
        /// <summary>
        /// 薪资
        /// </summary>
        public string Salary { get; set; }
        /// <summary>
        /// 所属市区
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 经验
        /// </summary>
        public string Experience { get; set; }
        /// <summary>
        /// 学历
        /// </summary>
        public string Education { get; set; }
        /// <summary>
        /// 职位链接
        /// </summary>
        public string Link { get; set; }
        /// <summary>
        /// 职位信息
        /// </summary>
        public string Info { get; set; }
        /// <summary>
        /// 工作地址
        /// </summary>
        public string WorkingAddress { get; set; }
    }
}
